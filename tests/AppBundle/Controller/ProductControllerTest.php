<?php
declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    public function testGetProductAction()
    {
        $title = 'Witcher 3';
        $price = 7.99;

        $client = $this->addProduct($title, $price);

        $testObject = json_decode($client->getResponse()->getContent());

        $this->assertContains($title, $testObject->title);
        $this->assertContains((string)$price, (string)$testObject->price);
        $this->assertNotEmpty($testObject->id);
    }

    public function testGetProductsPageAction()
    {
        $this->addProduct('Witcher 3', 15.99);
        $this->addProduct('Witcher 2', 2.99);
        $this->addProduct('Witcher 1', 1.99);
        $this->addProduct('FallOut', 5.0);
        $this->addProduct('FallOut 2', 3.99);

        $client = static::createClient();

        $client->request('get', '/products/1/page.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDeleteProductAction()
    {
        $title = 'Witcher 3';
        $price = 7.99;

        $client = $this->addProduct($title, $price);

        $product = json_decode($client->getResponse()->getContent());

        $productId = $product->id;

        $client = static::createClient();

        $client->request('DELETE', '/products/'.$productId.'.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

    public function testPostProductAction()
    {
        $title = 'Witcher 3';
        $price = 7.99;

        $client = $this->addProduct($title, $price);

        $testObject = json_decode($client->getResponse()->getContent());

        $this->assertContains($title, $testObject->title);
        $this->assertContains((string)$price, (string)$testObject->price);
        $this->assertNotEmpty($testObject->id);
    }

    /**
     * @param $title
     * @param $price
     *
     * @return Client
     */
    private function addProduct($title, $price): Client
    {
        $client = static::createClient();

        $object        = new \stdClass;
        $object->title = $title;
        $object->price = $price;

        $content = json_encode($object);

        $client->request('POST', '/products.json', [], [], [], $content);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        return $client;
    }
}
