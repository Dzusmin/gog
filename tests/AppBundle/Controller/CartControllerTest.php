<?php
declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CartControllerTest extends WebTestCase
{
    public function testPostCartAction()
    {
        $client = static::createClient();

        $client->request('POST', '/carts.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $testObject = json_decode($client->getResponse()->getContent());

        $this->assertContains('success',$testObject->message);
        $this->assertNotEmpty($testObject->cart->id);
    }

    public function testGetCartAction()
    {
        $client = static::createClient();

        $client->request('POST', '/carts.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $cart = json_decode($client->getResponse()->getContent());

        $this->assertNotEmpty($cart->cart->id);

        $cartId = $cart->cart->id;

        $client = static::createClient();

        $client->request('GET', '/carts/'.$cartId.'.json');

        $testObject = json_decode($client->getResponse()->getContent());

        $this->assertEquals($cartId, $testObject->id);
    }
}
