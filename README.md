Koszyk sklepowy
================

Projekt nie jest w 100% dokończony. 
Rzeczy, które bym jeszcze poprawił ale nie starczyło czasu:

1. zamienić generyczne exceptiony 
   na customowe, które faktycznie 
   mówią czemu się tak zepsuło

2. poprawić wyciąganie z bazy danych,
   tak żeby polegało na podawaniu command do handlera 

Próbowałem też na początku stworzyć projekt w 100% 
oparty na CQRS i ES ale zauważyłem że nie zmieszczę się w czasie, więc zrezygnowałem. Zachowałem próbę na branchu `ddd-cqrs-es`. 


``docker-compose up -d``



method              | method | path
--------------------|--------|---------------------------------------------
get_product         | GET    | /products/{productId}.{_format}            
get_products_page   | GET    | /products/{page}/page.{_format}            
delete_product      | DELETE | /products/{productId}.{_format}            
post_product        | POST   | /products.{_format}                        
get_cart            | GET    | /carts/{id}.{_format}                      
post_cart           | POST   | /carts.{_format}                           
delete_cart_product | DELETE | /carts/{cart}/products/{product}.{_format} 
post_cart_product   | POST   | /carts/{cart}/products/{product}.{_format} 
get_product_price   | GET    | /products/{productId}/price.{_format}      
patch_product_price | PATCH  | /products/{productId}/price.{_format}      
get_product_title   | GET    | /products/{productId}/title.{_format}      
patch_product_title | PATCH  | /products/{productId}/title.{_format}      



Requests by raw json payload 
