<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Domain\Cart\Command\ProductAddedToCart;
use AppBundle\Domain\Cart\Command\ProductRemovedFromCart;
use AppBundle\Repository\CartRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\HandlerAdapter;
use FOS\RestBundle\Controller\FOSRestController;

class ProductCartController extends FOSRestController
{
	/** @var HandlerAdapter */
	private $handlerAdapter;
	/** @var ProductRepository */
	private $productRepository;
	/** @var CartRepository */
	private $cartRepository;

	/**
	 * ProductCartController constructor.
	 *
	 * @param HandlerAdapter    $handlerAdapter
	 * @param ProductRepository $productRepository
	 * @param CartRepository    $cartRepository
	 */
	public function __construct(
		HandlerAdapter $handlerAdapter,
		ProductRepository $productRepository,
		CartRepository $cartRepository
	) {
		$this->handlerAdapter    = $handlerAdapter;
		$this->productRepository = $productRepository;
		$this->cartRepository    = $cartRepository;
	}

	public function deleteProductAction(string $cartId, string $productId)
	{
		$cart    = $this->cartRepository->findOneById($cartId);
		$product = $this->productRepository->findOneById($productId);

		$event = new ProductRemovedFromCart($cart, $product);

		$handlerMessage = $this->handlerAdapter->handle($event);

		$view = $this->view($handlerMessage);

		return $this->handleView($view);
	}

	public function postProductAction(string $cartId, string $productId)
	{
		$cart    = $this->cartRepository->findOneById($cartId);
		$product = $this->productRepository->findOneById($productId);

		$event = new ProductAddedToCart($cart, $product);

		$handlerMessage = $this->handlerAdapter->handle($event);

		$view = $this->view($handlerMessage);

		return $this->handleView($view);
	}
}
