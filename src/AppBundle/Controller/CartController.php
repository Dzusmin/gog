<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Domain\Cart\Command\CartWasOpened;
use AppBundle\Repository\CartRepository;
use AppBundle\Service\AggregateProducts;
use AppBundle\Service\HandlerAdapter;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 * @author  Jakub Jasiński <jakub.jasinski@juicecode.pl>
 */
class CartController extends FOSRestController
{
    /** @var HandlerAdapter */
    private $adapter;
    /** @var CartRepository */
    private $repository;
    /** @var AggregateProducts */
    private $aggregateProducts;

    /**
     * CartController constructor.
     *
     * @param HandlerAdapter    $adapter
     * @param CartRepository    $repository
     * @param AggregateProducts $aggregateProducts
     */
    public function __construct(
        HandlerAdapter $adapter,
        CartRepository $repository,
        AggregateProducts $aggregateProducts
    )
    {
        $this->adapter           = $adapter;
        $this->repository        = $repository;
        $this->aggregateProducts = $aggregateProducts;
    }

    public function getCartAction(string $id)
    {
        $cart = $this->repository->findOneById($id);

        $products = $this->aggregateProducts->aggregate($cart->getCartProductEvents());

        $cart->setProducts($products);

        $view = $this->view($cart);

        return $this->handleView($view);
    }

    public function postCartAction()
    {
        $cart = $this->adapter->handle(new CartWasOpened);

        $view = $this->view(['message' => 'success', 'cart' => $cart->getCart()]);

        return $this->handleView($view);
    }

}
