<?php
declare(strict_types=1);


namespace AppBundle\Controller;


use AppBundle\Domain\Product\Command\ProductPriceWasChanged;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\HandlerAdapter;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class ProductPriceController extends FOSRestController
{
	/** @var ProductRepository */
	private $repository;

	/** @var HandlerAdapter */
	private $handlerAdapter;

	/**
	 * ProductPriceController constructor.
	 *
	 * @param HandlerAdapter    $handlerAdapter
	 * @param ProductRepository $repository
	 */
	public function __construct(HandlerAdapter $handlerAdapter, ProductRepository $repository)
	{
		$this->repository     = $repository;
		$this->handlerAdapter = $handlerAdapter;
	}

	public function getPriceAction(string $productId)
	{
		$product = $this->repository->findOneById($productId);
		$view    = $this->view($product->getPrice());

		return $this->handleView($view);
	}

	public function patchPriceAction(Request $request, string $productId)
	{
		$product = $this->repository->findOneById($productId);
		$price   = (float)json_decode($request->getContent())->price;

		if ($price === null){
			//TODO: Poprawić exceptiony
			throw new \Exception();
		}

		$command = new ProductPriceWasChanged($product, $price);

		$handlerMessage = $this->handlerAdapter->handle($command);

		$view = $this->view($handlerMessage->getProduct());

		return $this->handleView($view);
	}
}
