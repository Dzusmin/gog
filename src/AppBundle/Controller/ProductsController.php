<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Domain\Product\Command\ProductWasAdded;
use AppBundle\Domain\Product\Command\ProductWasDeleted;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\HandlerAdapter;
use FOS\RestBundle\Controller\FOSRestController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class ProductsController extends FOSRestController
{
	const PAGINATION_LIMIT = 3;


	/** @var HandlerAdapter */
	private $handlerAdapter;

	/** @var ProductRepository */
	private $repository;

	/** @var PaginatorInterface */
	private $paginator;

	/**
	 * ProductsController constructor.
	 *
	 * @param HandlerAdapter     $handlerAdapter
	 * @param ProductRepository  $repository
	 * @param PaginatorInterface $paginator
	 */
	public function __construct(
		HandlerAdapter $handlerAdapter,
		ProductRepository $repository,
		PaginatorInterface $paginator
	) {
		$this->handlerAdapter = $handlerAdapter;
		$this->repository     = $repository;
		$this->paginator      = $paginator;
	}

	public function getProductAction(string $productId)
	{
		$product = $this->repository->findOneById($productId);

		$view = $this->view($product);

		return $this->handleView($view);
	}

	public function getProductsPageAction(int $page = 0)
	{
		$query = $this->repository->findAllToPaginate();

		$pagination = $this->paginator->paginate($query, $page, self::PAGINATION_LIMIT);

		$view = $this->view($pagination);

		return $this->handleView($view);
	}

	public function deleteProductAction(string $productId)
	{
		$product = $this->repository->findOneById($productId);

		$command = new ProductWasDeleted($product);

		$message = $this->handlerAdapter->handle($command);

		$view = $this->view($message->getProduct());

		return $this->handleView($view);
	}

	public function postProductAction(Request $request)
	{
		$parameterBag = json_decode($request->getContent());

		$title = (string)$parameterBag->title;
		$price = (float)$parameterBag->price;

		if ($title === null || $price === null)
		{
			//TODO: poprawić te exceptiony
			throw new \Exception();
		}

		$command = new ProductWasAdded($title, $price);

		$message = $this->handlerAdapter->handle($command);

		$view = $this->view($message->getProduct());

		return $this->handleView($view);
	}
}
