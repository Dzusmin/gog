<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Domain\Product\Command\ProductTitleWasChanged;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\HandlerAdapter;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class ProductTitleController extends FOSRestController
{
	/** @var HandlerAdapter */
	private $handlerAdapter;
	/** @var ProductRepository */
	private $repository;


	/**
	 * ProductTitleController constructor.
	 *
	 * @param HandlerAdapter    $handlerAdapter
	 * @param ProductRepository $repository
	 */
	public function __construct(HandlerAdapter $handlerAdapter, ProductRepository $repository)
	{
		$this->handlerAdapter = $handlerAdapter;
		$this->repository = $repository;
	}

	public function getTitleAction(string $productId)
    {
		$product = $this->repository->findOneById($productId);
		$view    = $this->view($product->getTitle());

		return $this->handleView($view);
    }

    public function patchTitleAction(Request $request, string $productId)
    {
		$product = $this->repository->findOneById($productId);
		$title   = (string)json_decode($request->getContent())->title;

		if ($title === null ){
			//TODO: poprawić exeptiony
			throw new \Exception();
		}

		$command = new ProductTitleWasChanged($product, $title);

		$handlerMessage = $this->handlerAdapter->handle($command);

		$view = $this->view($handlerMessage->getProduct());

		return $this->handleView($view);
    }
}
