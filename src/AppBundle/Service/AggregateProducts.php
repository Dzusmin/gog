<?php
declare(strict_types=1);


namespace AppBundle\Service;


use AppBundle\Entity\CartProductEvent;

class AggregateProducts
{
	/**
	 * @param CartProductEvent[] $products
	 *
	 * @return array
	 */
	public function aggregate($products = null): array
	{
		$aggregated = [];

		if ($products === null){
		    return [];
        }

		foreach ($products as $product) {
			switch ($product->getAction()){
				case 'add':
					$aggregated[$product->getId()] = $product;
					break;
				case 'remove':
					unset($aggregated[$product->getId()]);
					break;
			}
		}

		return $aggregated;
	}
}
