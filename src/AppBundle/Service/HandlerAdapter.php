<?php
declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;

class HandlerAdapter
{
    /** @var  HandlerInterface[] */
    private $handlers;

    public function registerHandler(HandlerInterface $handler): void
    {
        $this->handlers[] = $handler;
    }

    public function handle(CommandInterface $command): HandlerMessage
    {
        $hm = null;

        foreach ($this->handlers as $handler) {
            if ($handler->support($command)) {
                $hm = $handler->handle($command);
            }
        }

        if ($hm === null) {
            //TODO: poprawić exceptiony
            throw new \Exception;
        }

        return $hm;
    }
}
