<?php
declare(strict_types=1);

namespace AppBundle\Repository;

use AppBundle\Entity\Cart;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use function Sodium\crypto_box_publickey_from_secretkey;

class CartRepository extends EntityRepository
{
	public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
	{
		parent::__construct($em, $class);
	}

	public function save(Cart $cart)
	{
		$entityManager = $this->getEntityManager();

		$entityManager->persist($cart);
		$entityManager->flush();
	}

	public function findOneById(string $id): Cart
	{
		/** @var Cart $cart */
		$cart = $this->findOneBy(['id' => $id]);

		return $cart;
	}

	public function openCart(): Cart
	{
		$cart = (new Cart)->setStaus('open');
		$this->save($cart);

		return $cart;
	}

	public function closeCart(Cart $cart): Cart
	{
		if ($cart->getStaus() !== 'open') {
			//TODO: poprawić exceptiony
			throw new \Exception;
		}

		$cart->setStaus('close');
		$this->save($cart);

		return $cart;
	}
}
