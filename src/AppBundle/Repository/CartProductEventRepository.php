<?php
declare(strict_types=1);

namespace AppBundle\Repository;

use AppBundle\Entity\CartProductEvent;
use Doctrine\ORM\EntityRepository;

class CartProductEventRepository extends EntityRepository
{
    public function save(CartProductEvent $event)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($event);
        $entityManager->flush();
    }
}
