<?php
declare(strict_types=1);

namespace AppBundle\Repository;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
	public function save(Product $product)
	{
		$this->getEntityManager()->persist($product);
		$this->getEntityManager()->flush();
	}

	public function delete(Product $product)
	{
		$this->getEntityManager()->remove($product);
		$this->getEntityManager()->flush();
	}

	public function findOneById(string $id): Product
	{
		/** @var Product $product */
		$product = $this->findOneBy(['id' => $id]);

		return $product;
	}

	public function findAllToPaginate()
	{
		return $this->createQueryBuilder("product");
	}
}
