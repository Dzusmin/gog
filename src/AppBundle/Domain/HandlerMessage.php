<?php
declare(strict_types=1);

namespace AppBundle\Domain;

use AppBundle\Domain\Product\ProductCollection;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartProductEvent;
use AppBundle\Entity\Product;

class HandlerMessage
{
    /** @var  Cart */
    private $cart;

    /** @var  Product */
    private $product;

    /** @var  CartProductEvent */
    private $cartProductEvent;

    /** @var  ProductCollection */
    private $productCartCollection;

    public static function fromCart(Cart $cart): self
    {
        $message       = new self;
        $message->cart = $cart;

        return $message;
    }

    public static function fromProduct(Product $product): self
    {
        $message          = new self;
        $message->product = $product;

        return $message;
    }

    public static function fromCartProductEvent(CartProductEvent $cartProductEvent): self
    {
        $message = new self;

        $message->cartProductEvent = $cartProductEvent;
        $message->product          = $cartProductEvent->getProduct();
        $message->cart             = $cartProductEvent->getCart();

        return $message;
    }

    public static function fromCartProductCollection($collection): self
    {
        $message = new self;

        $message->productCartCollection = new ProductCollection($collection);
//        $message->product          = $collection->getProduct();
//        $message->cart             = $collection->getCart();

        return $message;
    }

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return CartProductEvent
	 */
	public function getCartProductEvent(): CartProductEvent
	{
		return $this->cartProductEvent;
	}

	/**
	 * @return ProductCollection
	 */
	public function getProductCartCollection(): ProductCollection
	{
		return $this->productCartCollection;
	}
}
