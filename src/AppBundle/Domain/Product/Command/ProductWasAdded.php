<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Product;

/**
 * Class ProductWasAdded
 *
 * @package AppBundle\Domain\Product\Command
 * @author Jakub Jasińksi <jakub.jasinski@juicecode.pl>
 */
class ProductWasAdded implements CommandInterface
{
	/** @var  Product */
	private $newProduct;

	/**
	 * ProductWasAdded constructor.
	 *
	 * @param string $title
	 * @param float  $price
	 *
	 * @throws \Exception
	 * @internal param Product $newProduct
	 */
	public function __construct(string $title, float $price)
	{
		if (strlen($title) < 3) {
			//TODO: poprawić to
			throw new \Exception();
		}

		if ($price < 0) {
			//TODO:poprawić to
			throw new \Exception();
		}

		$this->newProduct = (new Product())
			->setTitle($title)
			->setPrice($price);
	}

	/**
	 * @return Product
	 */
	public function getNewProduct(): Product
	{
		return $this->newProduct;
	}
}