<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Product;

class ProductWasDeleted implements CommandInterface
{
	/** @var  Product */
	private $product;

	/**
	 * ProductWasDeleted constructor.
	 *
	 * @param Product $product
	 */
	public function __construct(Product $product)
	{
		$this->product = $product;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}
}
