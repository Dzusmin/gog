<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Product;

/**
 * Class ProductPriceWasChanged
 *
 * @package AppBundle\Domain\Product\Command
 */
class ProductPriceWasChanged implements CommandInterface
{
	/** @var  Product */
	private $product;

	/** @var  float */
	private $newPrice;

	/**
	 * ProductPriceWasChanged constructor.
	 *
	 * @param Product $product
	 * @param float   $newPrice
	 *
	 * @throws \Exception
	 */
	public function __construct(Product $product, float $newPrice)
	{
		$this->product = $product;

		if ($newPrice < 0) {
			//TODO: Poprawić to
			throw new \Exception();
		}
		$this->newPrice = $newPrice;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return float
	 */
	public function getNewPrice(): float
	{
		return $this->newPrice;
	}
}
