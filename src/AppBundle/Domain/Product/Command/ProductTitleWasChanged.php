<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Product;

class ProductTitleWasChanged implements CommandInterface
{
	/** @var  Product */
	private $product;

	/** @var  string */
	private $newTitle;

	/**
	 * ProductTitleWasChanged constructor.
	 *
	 * @param Product $product
	 * @param string  $newTitle
	 */
	public function __construct(Product $product, $newTitle)
	{
		$this->product  = $product;
		$this->newTitle = $newTitle;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return string
	 */
	public function getNewTitle(): string
	{
		return $this->newTitle;
	}
}