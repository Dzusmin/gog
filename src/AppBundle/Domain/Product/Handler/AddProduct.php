<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Handler;


use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Domain\Product\Command\ProductWasAdded;
use AppBundle\Repository\ProductRepository;

class AddProduct implements HandlerInterface
{
	/** @var ProductRepository */
	private $repository;


	/**
	 * AddProduct constructor.
	 *
	 * @param ProductRepository $repository
	 */
	public function __construct(ProductRepository $repository)
	{
		$this->repository = $repository;
	}

	public function support(CommandInterface $command): bool
	{
		return $command instanceof ProductWasAdded;
	}

	public function handle(CommandInterface $command): HandlerMessage
	{
		/** @var ProductWasAdded $command */
		$product = $command->getNewProduct();

		$this->repository->save($product);

		return HandlerMessage::fromProduct($product);
	}
}