<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Handler;


use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Domain\Product\Command\ProductPriceWasChanged;
use AppBundle\Repository\ProductRepository;

class UpdateProductPrice implements HandlerInterface
{
	/** @var ProductRepository */
	private $repository;

	/**
	 * UpdateProductPrice constructor.
	 *
	 * @param ProductRepository $repository
	 */
	public function __construct(ProductRepository $repository)
	{
		$this->repository = $repository;
	}

	public function support(CommandInterface $command): bool
	{
		return $command instanceof ProductPriceWasChanged;
	}

	public function handle(CommandInterface $command): HandlerMessage
	{
		/** @var ProductPriceWasChanged $command */
		$price   = $command->getNewPrice();
		$product = $command->getProduct()->setPrice($price);
		$this->repository->save($product);

		return HandlerMessage::fromProduct($product);
	}
}