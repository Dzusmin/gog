<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Handler;


use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Domain\Product\Command\ProductTitleWasChanged;
use AppBundle\Repository\ProductRepository;

class UpdateProductTitle implements HandlerInterface
{
	/** @var ProductRepository */
	private $repository;

	/**
	 * UpdateProductTitle constructor.
	 *
	 * @param ProductRepository $repository
	 */
	public function __construct(ProductRepository $repository)
	{
		$this->repository = $repository;
	}

	public function support(CommandInterface $command): bool
	{
		return $command instanceof ProductTitleWasChanged;
	}

	public function handle(CommandInterface $command): HandlerMessage
	{
		/** @var ProductTitleWasChanged $command */
		$product = $command->getProduct();
		$product->setTitle($command->getNewTitle());
		$this->repository->save($product);

		return HandlerMessage::fromProduct($product);
	}
}