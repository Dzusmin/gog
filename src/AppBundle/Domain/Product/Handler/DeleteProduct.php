<?php
declare(strict_types=1);


namespace AppBundle\Domain\Product\Handler;


use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Domain\Product\Command\ProductWasDeleted;
use AppBundle\Repository\ProductRepository;

class DeleteProduct implements HandlerInterface
{
	/** @var ProductRepository */
	private $repository;

	/**
	 * DeleteProduct constructor.
	 *
	 * @param ProductRepository $repository
	 */
	public function __construct(ProductRepository $repository)
	{
		$this->repository = $repository;
	}

	public function support(CommandInterface $command): bool
	{
		return $command instanceof ProductWasDeleted;
	}

	public function handle(CommandInterface $command): HandlerMessage
	{
		/** @var ProductWasDeleted $command */
		$product = $command->getProduct();

		$this->repository->delete($product);

		return HandlerMessage::fromProduct($product);
	}
}