<?php
declare(strict_types=1);

namespace AppBundle\Domain\Cart\Handler;

use AppBundle\Domain\Cart\Command\ProductRemovedFromCart;
use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Entity\CartProductEvent;
use AppBundle\Repository\CartProductEventRepository;

class RemoveProductFromCart implements HandlerInterface
{
    /** @var CartProductEventRepository */
    private $cartProductEventRepository;

    /**
     * RemoveProductFromCart constructor.
     *
     * @param CartProductEventRepository $cartProductEventRepository
     */
    public function __construct(CartProductEventRepository $cartProductEventRepository)
    {
        $this->cartProductEventRepository = $cartProductEventRepository;
    }

    public function support(CommandInterface $command): bool
    {
        return $command instanceof ProductRemovedFromCart;
    }

    public function handle(CommandInterface $command): HandlerMessage
    {
        /** @var ProductRemovedFromCart $command */
        $event = (new CartProductEvent)
            ->setProduct($command->getProduct())
            ->setCart($command->getCart())
            ->setAction('remove')
		->setEventAt(new \DateTime('now'));

        $this->cartProductEventRepository->save($event);

        return HandlerMessage::fromCartProductEvent($event);

    }
}
