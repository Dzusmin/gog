<?php
declare(strict_types=1);

namespace AppBundle\Domain\Cart\Handler;

use AppBundle\Domain\Cart\Command\ProductAddedToCart;
use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Entity\CartProductEvent;
use AppBundle\Repository\CartProductEventRepository;

class AddProductToCart implements HandlerInterface
{
    /** @var CartProductEventRepository */
    private $cartProductEventRepository;

    /**
     * AddProductToCart constructor.
     *
     * @param CartProductEventRepository $cartProductEventRepository
     */
    public function __construct(CartProductEventRepository $cartProductEventRepository)
    {
        $this->cartProductEventRepository = $cartProductEventRepository;
    }

    public function support(CommandInterface $command): bool
    {
        return $command instanceof ProductAddedToCart;
    }

    public function handle(CommandInterface $command): HandlerMessage
    {
        /** @var ProductAddedToCart $command */
        $event = (new CartProductEvent)
            ->setProduct($command->getProduct())
            ->setCart($command->getCart())
            ->setAction('add')
			->setEventAt(new \DateTime('now'));

        $this->cartProductEventRepository->save($event);

        return HandlerMessage::fromCartProductEvent($event);
    }
}
