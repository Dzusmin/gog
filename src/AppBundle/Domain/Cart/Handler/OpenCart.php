<?php
declare(strict_types=1);


namespace AppBundle\Domain\Cart\Handler;


use AppBundle\Domain\Cart\Command\CartWasOpened;
use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Entity\Cart;
use AppBundle\Repository\CartRepository;

class OpenCart implements HandlerInterface
{
	/** @var CartRepository */
	private $cartRepository;

	/**
	 * OpenCart constructor.
	 *
	 * @param CartRepository $cartRepository
	 */
	public function __construct(CartRepository $cartRepository)
	{
		$this->cartRepository = $cartRepository;
	}

	public function support(CommandInterface $command): bool
	{
		return $command instanceof CartWasOpened;
	}

	public function handle(CommandInterface $command): HandlerMessage
	{
		$cart = $this->cartRepository->openCart();

		return HandlerMessage::fromCart($cart);
	}
}
