<?php
declare(strict_types=1);

namespace AppBundle\Domain\Cart\Handler;

use AppBundle\Domain\Cart\Command\CartWasClosed;
use AppBundle\Domain\CommandInterface;
use AppBundle\Domain\HandlerInterface;
use AppBundle\Domain\HandlerMessage;
use AppBundle\Repository\CartRepository;

class CloseCart implements HandlerInterface
{
    /** @var CartRepository */
    private $cartRepository;

    /**
     * CloseCart constructor.
     *
     * @param CartRepository $cartRepository
     */
    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function support(CommandInterface $command): bool
    {
        return $command instanceof CartWasClosed;
    }

    public function handle(CommandInterface $command): HandlerMessage
    {
        /** @var CartWasClosed $command */
        $closedCard = $this->cartRepository->closeCart($command->getCart());

        return HandlerMessage::fromCart($closedCard);
    }
}
