<?php
declare(strict_types=1);


namespace AppBundle\Domain\Cart\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Cart;
use AppBundle\Entity\Product;

/**
 * Class ProductAddedToCart
 *
 * @package AppBundle\Domain\Cart\Command
 * @author Jakub Jasiński <jakub.jasinski@juicecode.pl>
 */
class ProductAddedToCart implements CommandInterface
{
	/** @var  Cart */
	private $cart;

	/** @var  Product */
	private $product;

	/** @var  \DateTime */
	private $addedAt;

	/**
	 * ProductAddedToCart constructor.
	 *
	 * @param Cart    $cart
	 * @param Product $product
	 */
	public function __construct(Cart $cart, Product $product)
	{
		$this->cart    = $cart;
		$this->product = $product;
		$this->addedAt = new \DateTime('now');
	}

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return \DateTime
	 */
	public function getAddedAt(): \DateTime
	{
		return $this->addedAt;
	}

}
