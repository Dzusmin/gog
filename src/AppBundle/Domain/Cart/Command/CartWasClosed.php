<?php
declare(strict_types=1);


namespace AppBundle\Domain\Cart\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Cart;

class CartWasClosed implements CommandInterface
{
	/** @var  Cart */
	private $cart;

	/**
	 * CartWasClosed constructor.
	 *
	 * @param Cart $cart
	 */
	public function __construct(Cart $cart)
	{
		$this->cart = $cart;
	}

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}
}
