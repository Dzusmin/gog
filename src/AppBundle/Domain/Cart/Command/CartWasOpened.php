<?php
declare(strict_types=1);


namespace AppBundle\Domain\Cart\Command;


use AppBundle\Domain\CommandInterface;

class CartWasOpened implements CommandInterface
{
}
