<?php
declare(strict_types=1);


namespace AppBundle\Domain\Cart\Command;


use AppBundle\Domain\CommandInterface;
use AppBundle\Entity\Cart;
use AppBundle\Entity\Product;

/**
 * Class ProductRemovedFromCart
 *
 * @package AppBundle\Domain\Cart\Command
 * @author Jakub Jasiński <jakub.jasinski@juicecode.pl>
 */
class ProductRemovedFromCart implements CommandInterface
{
	/** @var  Cart */
	private $cart;

	/** @var  Product */
	private $product;

	/** @var  \DateTime */
	private $removedAt;

	/**
	 * ProductAddedToCart constructor.
	 *
	 * @param Cart    $cart
	 * @param Product $product
	 */
	public function __construct(Cart $cart, Product $product)
	{
		$this->cart      = $cart;
		$this->product   = $product;
		$this->removedAt = new \DateTime('now');
	}

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return \DateTime
	 */
	public function getRemovedAt(): \DateTime
	{
		return $this->removedAt;
	}

}