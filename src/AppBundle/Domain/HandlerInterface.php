<?php
declare(strict_types=1);


namespace AppBundle\Domain;

interface HandlerInterface
{
	public function support(CommandInterface $command): bool;

	public function handle(CommandInterface $command): HandlerMessage;
}
