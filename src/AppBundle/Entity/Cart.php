<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Ramsey\Uuid\Uuid;
use JMS\Serializer\Annotation as JMS;


/**
 * Class Cart
 *
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CartRepository")
 * @ORM\Table(name="cart")
 *
 * @package AppBundle\Entity
 * @author Jakub Jasiński <jakub.jasinski@juicecode.pl>
 */
class Cart
{
	use Timestampable;

	/**
	 * @JMS\Type("uuid")
	 *
	 * @ORM\Id
	 * @ORM\Column(type="uuid", unique=true)
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
	 * @var  Uuid
	 */
	private $id;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $staus;

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartProductEvent", mappedBy="cart")
	 * @var  CartProductEvent[]
	 */
	private $cartProductEvents;

	/**
	 * @var array
	 */
	private $products;

	/**
	 * @return Uuid
	 */
	public function getId(): Uuid
	{
		return $this->id;
	}

	/**
	 * @param Uuid $id
	 *
	 * @return Cart
	 */
	public function setId(Uuid $id): self
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getStaus()
	{
		return $this->staus;
	}

	/**
	 * @param mixed $staus
	 *
	 * @return Cart
	 */
	public function setStaus($staus)
	{
		$this->staus = $staus;

		return $this;
	}

	/**
	 * @return CartProductEvent[]
	 */
	public function getCartProductEvents()
	{
		return $this->cartProductEvents;
	}

	/**
	 * @param CartProductEvent[] $cartProductEvents
	 *
	 * @return Cart
	 */
	public function setCartProductEvents(array $cartProductEvents): self
	{
		$this->cartProductEvents = $cartProductEvents;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return Cart
	 */
	public function setCreatedAt(\DateTime $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdatedAt(): \DateTime
	{
		return $this->updatedAt;
	}

	/**
	 * @param \DateTime $updatedAt
	 *
	 * @return Cart
	 */
	public function setUpdatedAt(\DateTime $updatedAt): self
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getProducts(): array
	{
		return $this->products;
	}

	/**
	 * @param array $products
	 *
	 * @return Cart
	 */
	public function setProducts(array $products): self
	{
		$this->products = $products;

		return $this;
	}
}
