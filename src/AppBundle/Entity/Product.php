<?php
declare(strict_types=1);


namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\Timestampable;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * Class Product
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 *
 * @package AppBundle\Entity
 * @author  Jakub Jasisńki <jakub.jasinski@juicecode.pl>
 */
class Product
{
	use Timestampable;

	/**
	 * @JMS\Type("uuid")
	 *
	 * @ORM\Id
	 * @ORM\Column(type="uuid", unique=true)
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
	 *
	 * @var  Uuid
	 */
	private $id;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="decimal", precision=10, scale=2 )
	 * @var float
	 */
	private $price;

	/**
     * @JMS\Exclude()
     *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartProductEvent", mappedBy="product")
	 * @var CartProductEvent[]
	 */
	private $cartProductEvents;

	/**
	 * Product constructor.
	 */
	public function __construct()
	{
		$this->cartProductEvents = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return Product
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 *
	 * @return Product
	 */
	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 *
	 * @return Product
	 */
	public function setPrice(float $price): self
	{
		$this->price = $price;

		return $this;
	}

	/**
	 * @return CartProductEvent[]
	 */
	public function getCartProductEvents(): array
	{
		return $this->cartProductEvents;
	}

	/**
	 * @param CartProductEvent[] $cartProductEvents
	 *
	 * @return Product
	 */
	public function setCartProductEvents(array $cartProductEvents): self
	{
		$this->cartProductEvents = $cartProductEvents;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return Product
	 */
	public function setCreatedAt(\DateTime $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdatedAt(): \DateTime
	{
		return $this->updatedAt;
	}

	/**
	 * @param \DateTime $updatedAt
	 *
	 * @return Product
	 */
	public function setUpdatedAt(\DateTime $updatedAt): self
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}
}
