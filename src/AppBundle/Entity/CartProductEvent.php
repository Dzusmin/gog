<?php
declare(strict_types=1);


namespace AppBundle\Entity;


use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;


/**
 * Class CartProductEvent
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CartProductEventRepository")
 * @ORM\Table(name="cart_product_event")
 *
 * @package AppBundle\Entity
 * @author Jakub Jasiński <jakub.jasinski@juicecode.pl>
 */
class CartProductEvent
{
	/**
	 * @JMS\Type("uuid")
	 *
	 * @ORM\Id
	 * @ORM\Column(type="uuid", unique=true)
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
	 *
	 * @var  Uuid
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cart", inversedBy="cartProductEvents")
	 * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
	 * @var  Cart
	 */
	private $cart;

	/**
	 * /**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="cartProductEvents")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 * @var  Product
	 */
	private $product;

	/**
	 * @ORM\Column(type="string")
	 * @var  string
	 */
	private $action;

	/**
	 * @ORM\Column(type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 * @var  \DateTime
	 */
	private $eventAt;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return CartProductEvent
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}

	/**
	 * @param Cart $cart
	 *
	 * @return CartProductEvent
	 */
	public function setCart(Cart $cart): self
	{
		$this->cart = $cart;

		return $this;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @param Product $product
	 *
	 * @return CartProductEvent
	 */
	public function setProduct(Product $product): self
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * @param string $action
	 *
	 * @return CartProductEvent
	 */
	public function setAction(string $action): self
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getEventAt(): \DateTime
	{
		return $this->eventAt;
	}

	/**
	 * @param \DateTime $eventAt
	 *
	 * @return CartProductEvent
	 */
	public function setEventAt(\DateTime $eventAt): self
	{
		$this->eventAt = $eventAt;

		return $this;
	}
}
