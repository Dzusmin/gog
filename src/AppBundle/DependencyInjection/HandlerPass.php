<?php
declare(strict_types=1);

namespace AppBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class HandlerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('adapter.handler.domain')) {
            return;
        }

        $definition = $container->findDefinition('adapter.handler.domain');

        $taggedServices = $container->findTaggedServiceIds('domain.handler');

        foreach ($taggedServices as $taggedService => $tags) {
            $definition->addMethodCall('registerHandler', [new Reference($taggedService)]);
        }
    }
}
